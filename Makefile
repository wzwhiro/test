obj-$(CONFIG_NOVA_FS) += nova.o

nova-y := balloc.o bbuild.o checksum.o dax.o dir.o file.o gc.o inode.o ioctl.o \
        journal.o log.o mprotect.o namei.o parity.o rebuild.o snapshot.o stats.o \
        super.o symlink.o sysfs.o perf.o \
	init.o dma.o prep.o dca.o sys.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=`pwd` modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=`pwd` clean
